/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Rocío
 */
@ManagedBean(name = "cal")
@RequestScoped
public class Calculos {

    private double ope1;
    private double ope2;
    private String operacion;
    private double resultado;

    public double getOpe1() {
        return ope1;
    }

    public void setOpe1(double ope1) {
        this.ope1 = ope1;
    }

    public double getOpe2() {
        return ope2;
    }

    public void setOpe2(double ope2) {
        this.ope2 = ope2;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }
    

    /**
     * Creates a new instance of Calculos
     */
    public Calculos() {
    }

    public void operaciones() {
        switch (operacion) {
            case "suma":
                resultado = getOpe1() + getOpe2();
                System.out.println("suma"+resultado+ope1+ope2);
               break;
            case "resta":
                resultado = ope1 - ope2;
                System.out.println(resultado);
                break;
            case "multiplicacion":
                resultado = ope1 * ope2;
                System.out.println(resultado);
                break;
            case "division":
                resultado = ope1 / ope2;
                System.out.println(resultado);
                break;
        }
    }

}
